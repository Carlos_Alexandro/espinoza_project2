	

# This function creates a list of planes that will be sorted by its submission
# time in reverse because its easy to pop from a list from the front.
# We seperate the data by the ID, Submission time, Requested time of take off
# and duration time.
def createListOfPlanes():
	file = open("test.txt", "r")

	try:
		listOfAirplanes = []
		for line in file:
			data = line.rstrip().split(", ")
			plane = {"airplane_id":data[0], 
			"submission_time":int(data[1]), 
			"requested_time":int(data[2]), 
			"duration_time":int(data[3])}

			# Appending the list from the information that its given 
			listOfAirplanes.append(plane)

	except IOError:
		print("File could not be read! Give me another one.")	
	
	# Creating a new list by using lambda and the submission time 
	# of the list. Also, reversing the list by having the highest
	# value of the submission time at the front of the list.
	newList = sorted(listOfAirplanes, key=lambda plane: (plane['submission_time'],plane['requested_time']), reverse=True)
	print(newList)

	time = 0

# Here we run the simulation using the sorted list from before, also
# starting our timer so we can keep track of time passing by.
def simulation(newList):
	time = 0

	while





if __name__ == "__main__":
	createListOfPlanes()
	# simulation()