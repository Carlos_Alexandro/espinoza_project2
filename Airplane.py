def __inti__(self):
    # Start the values for each plane object at zero and empty 
    self.ID = ""
    self.submissionTime = 0
    self.requestedTime = 0
    self.timeDuration = 0

    # Create variable to check the times when airplanes start and 
    # when they are done taking off.
    self.flightStart = 0
    self.flightEnd = 0

def __init__(requestedTime, timeDuration):
    self.flightStart = requestedTime
    self.flightEnd = int(self.requestedTime) + int(self.timeDuration)


# This methods gets all the information for the planes and we do not need
# to change it since the information from the planes will not changed to 
# begin with, onle when the flights starts to move and when it takes off.
def getID(self.ID)
    return str(self.ID)
def getSubmissionTime(self.submissionTime)
    return int(self.submissionTime)
def getRequestedTime(self.requestedTime)
    return int(self.requiredTime)
def getTimeDuration(self.timeDuration)
    return int(self.timeDuration)

# This methods have the times for the planes starting and ending times 
# because this values do change, they also have setters that can be
# used when they are needed.    
def getFlightStart(self.flightStart)
    return int(self.flightStart)
def getFlightEnd(self.flightEnd)
    return int(self.flightEnd)

# Creating the setters for the new start times and end times for each plane
# Because sometimes if the schedule does not allow a plane to take off in
# the time its requested, then the start and end time has to be changed
# so the schedule can be updated.
def setFlightEnd(plane, time)
    self.flightEnd = time
def setFightStart(plane, time)
    self.flightStart = time
